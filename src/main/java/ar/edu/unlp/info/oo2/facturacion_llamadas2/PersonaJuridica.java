package ar.edu.unlp.info.oo2.facturacion_llamadas2;

public class PersonaJuridica extends Persona {
	private String cuit;
	
	public PersonaJuridica(String cuit, String nomyAp) {
		this.nombreYapellido = nomyAp;
		this.cuit = cuit;
		this.descuento = 0.15;
	}
	public String getCuit() {
		return cuit;
	}
	public void setCuit(String cuit) {
		this.cuit = cuit;
	}

}
