package ar.edu.unlp.info.oo2.facturacion_llamadas2;

public class PersonaFisica extends Persona{
	private String documento;
	
	public PersonaFisica(String documento, String nomyAp) {
		this.nombreYapellido = nomyAp;
		this.documento = documento;
		this.descuento = 0;
	}
	public String getDocumento() {
		return documento;
	}
	public void setDocumento(String doc) {
		this.documento = doc;
	}

}
