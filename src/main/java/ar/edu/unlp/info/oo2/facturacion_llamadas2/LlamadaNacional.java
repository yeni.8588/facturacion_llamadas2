package ar.edu.unlp.info.oo2.facturacion_llamadas2;

public class LlamadaNacional extends Llamada {

	public LlamadaNacional (Persona emisor, Persona remitente, int dur) {
		this.emisor = emisor;
		this.remitente = remitente;
		this.duracion = dur;
		this.costo = 3;
	}
	
}
