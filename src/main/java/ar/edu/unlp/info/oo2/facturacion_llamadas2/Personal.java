package ar.edu.unlp.info.oo2.facturacion_llamadas2;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Personal {
	List<Persona> listaClientes = new ArrayList<Persona>();
	List<Llamada> listaLlamadas = new ArrayList<Llamada>();
	GuiaTelefonica guiaTelefonica = new GuiaTelefonica();
	
	public boolean agregarTelefono(String str) {
		return this.guiaTelefonica.agregarTelefono(str);
	}
	
	public Persona registrarUsuario(Persona persona) {
		persona.setSistema(this);
		persona.setTelefono(guiaTelefonica.nuevoNumero());
		listaClientes.add(persona);
		return persona;	
	}
	
	public boolean eliminarUsuario(Persona p) {
		if (this.existeUsuario(p)) {
			this.listaClientes = listaClientes.stream().filter(u -> u != p).collect(Collectors.toList());
			this.guiaTelefonica.agregarTelefono(p.getTelefono());
			return true;
		}
		return false;
		
	}
	
	public Llamada registrarLlamada(Llamada llamada) {
		listaLlamadas.add(llamada);
		llamada.emisor.misLlamadas.add(llamada);
		return llamada;
	}
	
	public double calcularMontoTotalLlamadas(Persona persona) {
		double c = 0;
		Persona aux = listaClientes.stream().filter(pp -> pp.getTelefono() 	== persona.getTelefono()).findFirst().orElse(null);
		if (aux != null) {		
			c = persona.totalMisLlamadas();
		}
		return c;
	}

	public int cantidadDeUsuarios() {
		return listaClientes.size();
	}

	public boolean existeUsuario(Persona persona) {
		return listaClientes.contains(persona);
	}
	
}
