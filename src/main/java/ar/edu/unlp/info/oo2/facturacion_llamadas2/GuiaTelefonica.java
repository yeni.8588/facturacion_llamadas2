package ar.edu.unlp.info.oo2.facturacion_llamadas2;

import java.util.TreeSet;
import java.util.SortedSet;

public class GuiaTelefonica {
	private SortedSet<String> guia; 

	public GuiaTelefonica() {
		this.guia = new TreeSet<String>();
	}
	
	public SortedSet<String> getGuia() {
		return guia;
	}

	public void setGuia(SortedSet<String> guia) {
		this.guia = guia;
	}
	
	public String nuevoNumero() {
		String tel = this.guia.last();
		this.guia.remove(tel);
		return tel;
	}
	
	public boolean buscarTelefono(String telefono) {
		return this.guia.contains(telefono);
	}
	
	public boolean agregarTelefono(String telefono) {
		if (!this.buscarTelefono(telefono)) {
			this.guia.add(telefono);
			return true;
		}else {
			return false;
		}
	}
	
}


