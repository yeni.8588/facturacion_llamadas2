package ar.edu.unlp.info.oo2.facturacion_llamadas2;

import java.util.ArrayList;
import java.util.List;

public class Persona {
	protected List<Llamada> misLlamadas = new ArrayList<Llamada>();
	protected String nombreYapellido;
	protected String telefono;
	protected Personal sistema;
	protected double descuento;
	
	public Persona() {		
	}
		
	public List<Llamada> getMisLlamadas() {
		return misLlamadas;
	}
	public void setMisLLamadas(List<Llamada> lista1) {
		this.misLlamadas = lista1;
	}

	public String getNombreyApellido() {
		return nombreYapellido;
	}
	public void setNombreyApellido(String nya) {
		this.nombreYapellido = nya;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String tel) {
		this.telefono = tel;
	}
	
	public double totalMisLlamadas() {
		double auxc = 0;	
		for (Llamada l : this.misLlamadas) {
			auxc += l.costoLLamada();		
		}
		auxc -= auxc*this.descuento;
		return auxc;
	}
	
	public double getDescuento() {
		return descuento;
	}

	public void setDescuento(double descuento) {
		this.descuento = descuento;
	}
	
	public void setSistema(Personal sistema) {
		this.sistema = sistema;
	}
	
}
