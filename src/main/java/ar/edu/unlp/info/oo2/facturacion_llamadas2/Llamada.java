package ar.edu.unlp.info.oo2.facturacion_llamadas2;


public class Llamada {
	protected Persona emisor;
	protected Persona remitente;
	protected int duracion;
	protected double costo;
	
	public Llamada() {	
	}

	public Persona getEmisor() {
		return this.emisor;
	}
	
	public Persona getRemitente() {
		return remitente;
	}
	
	public int getDuracion() {
		return this.duracion;
	}
	
	public double getCosto() {
		return this.costo;
	}
	public void setCosto(int costo) {
		this.costo = costo;
	}
	
	public double costoLLamada() {
		return this.duracion *this.costo + (this.duracion*this.costo*0.21);
	}
	
}