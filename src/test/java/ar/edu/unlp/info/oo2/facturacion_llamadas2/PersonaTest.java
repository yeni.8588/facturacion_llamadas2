package ar.edu.unlp.info.oo2.facturacion_llamadas2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PersonaTest {
	
	Personal sistema;
	Persona emisorPersonaFisca, remitentePersonaFisica, emisorPersonaJuridica, remitentePersonaJuridica;
	
	@BeforeEach
	public void setUp() {
		this.sistema = new Personal();
		this.sistema.agregarTelefono("2214444554");
		this.sistema.agregarTelefono("2214444555");
		this.sistema.agregarTelefono("2214444556");
		this.sistema.agregarTelefono("2214444557");
		
		this.emisorPersonaFisca = sistema.registrarUsuario(new PersonaFisica("11555666", "Marcelo Tinelli"));
		this.remitentePersonaFisica = sistema.registrarUsuario(new PersonaFisica("00000001", "Mirtha Legrand"));
		this.emisorPersonaJuridica = sistema.registrarUsuario(new PersonaJuridica("17555222", "Felfort"));
		this.remitentePersonaJuridica = sistema.registrarUsuario(new PersonaJuridica("25765432", "Moovistar"));
		
		this.sistema.registrarLlamada(new LlamadaNacional(emisorPersonaJuridica, remitentePersonaFisica, 10));
		this.sistema.registrarLlamada(new LlamadaInternacional(emisorPersonaJuridica, remitentePersonaFisica, 8));
		this.sistema.registrarLlamada(new LlamadaNacional(emisorPersonaJuridica, remitentePersonaJuridica, 5));
		this.sistema.registrarLlamada(new LlamadaInternacional(emisorPersonaJuridica, remitentePersonaJuridica, 7));
		this.sistema.registrarLlamada(new LlamadaNacional(emisorPersonaFisca, remitentePersonaFisica, 15));
		this.sistema.registrarLlamada(new LlamadaInternacional(emisorPersonaFisca, remitentePersonaFisica, 45));
		this.sistema.registrarLlamada(new LlamadaNacional(emisorPersonaFisca, remitentePersonaJuridica, 13));
		this.sistema.registrarLlamada(new LlamadaInternacional(emisorPersonaFisca, remitentePersonaJuridica, 17));
		
	}

	@Test
	void testcalcularMontoTotalLlamadas() {
		assertEquals(this.sistema.calcularMontoTotalLlamadas(emisorPersonaFisca), 15105.640000000001);
		assertEquals(this.sistema.calcularMontoTotalLlamadas(emisorPersonaJuridica), 3131.7825);
		assertEquals(this.sistema.calcularMontoTotalLlamadas(remitentePersonaFisica), 0);
		assertEquals(this.sistema.calcularMontoTotalLlamadas(remitentePersonaJuridica), 0);
	}
	
	@Test
	void testAgregarUsuario() {
		assertEquals(this.sistema.cantidadDeUsuarios(), 4);
		this.sistema.agregarTelefono("2214444558"); 
		Persona nuevaPersona = this.sistema.registrarUsuario(new PersonaFisica("2444555","Chiche Gelblung"));
		
		assertEquals(this.sistema.cantidadDeUsuarios(), 5);
		assertTrue(this.sistema.existeUsuario(nuevaPersona));
		
	}
	
	@Test
	void testEliminarUsuario() {
		assertEquals(this.sistema.cantidadDeUsuarios(), 4);
		assertTrue(this.sistema.existeUsuario(emisorPersonaFisca));
		this.sistema.eliminarUsuario(emisorPersonaFisca);
		assertEquals(this.sistema.cantidadDeUsuarios(), 3);
		assertFalse(this.sistema.existeUsuario(emisorPersonaFisca));
	}

}